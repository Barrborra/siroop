Siroop Demo
===========
![Demo Image](https://bytebucket.org/Barrborra/siroop/raw/b21793a88abc5606db5798cdcc3da0ca0a78b701/demos/demo_image.png?token=8f2a7ef0ee0fb48a0717035f0e0e8ad6339ff686)

Demo video is located at [Dropbox](https://dl.dropboxusercontent.com/u/55379463/demo_video.mov)

### Scope of Test Task:
Read the file data/feed.xml and store it in a DB or in Memory if easier for you. If in memory, you might want to shorten the file before.
Make the most important info accessible in a browser, at least title, description (short and long), price.
You can choose how to implement it but do not use too many frameworks, show that you can do file processing and building the frontend on your own. sass, bootstrap, etc is fine. Do not spend too long overall, it doesn't have to be finished, show us your strengths.

### Requirements and Dependancies:
The code presented in this repository is tested against python 3.x and sqlite database, which is configured in `config.py` file. It means you may freely use a different database, when you change database configuration in `config.py`. file. Your database have to be installed and running. Target folder for database migrations is also defined in `config.py` file. For this little demo task I chose SQLite, because it doesn't have any third party dependencies as Postgres does, but I would never use it in production.

Required Python dependencies like Flask version and list of other libraries, can be found in `requirements.txt`.
Python 3.x and pip3 is *required*.

### How To Run:
Setup is automated by shell script and can be invoked by running:

```
./setup.sh
```

Starting the application:

```
python3 run.py
```

App is running at http://127.0.0.1:5000/ (Press CTRL+C to quit).

### Notable Features:
- Jinja2 inheritance, blocks and control structures enable reusing of existing HTML code and dynamic generated content (i.e.: filling content with eshop items from database and pagination by predefined number of items).
- Parsing of product feed is unit tested with database mock.
- Project contains scripts for creating, upgrading, downgrading and migrating predefined database and setting up the project

### List of References:
Following sources inspired me during solving the test task

- [Scripts for creating, upgrading, downgrading and migrating of database, pagination](http://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database)