import xml.etree.ElementTree

from app import db, models


class Parser:
    def __init__(self, db=db):
        self.db = db

    def parse_product_feed(self, filepath):
        root = xml.etree.ElementTree.parse(filepath).getroot()

        for xml_product in root.findall('product'):
            object_product = models.Product()

            for xml_attributes in xml_product:
                if (xml_attributes.tag == 'features'):
                    for xml_feature in xml_attributes.iter("feature"):
                        if xml_feature.find('label') is not None:
                            feature = models.Feature(label=xml_feature.find('label').text,
                                                     value=xml_feature.find('value').text, product=object_product)
                            self.db.session.add(feature)
                else:
                    setattr(object_product, xml_attributes.tag, xml_attributes.text)
            self.db.session.add(object_product)
            self.db.session.commit()
