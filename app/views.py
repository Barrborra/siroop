from flask import render_template, flash, redirect
from app import app, models
from config import PRODUCTS_PER_PAGE
from random import randint


@app.route('/', methods=['GET'])
@app.route('/index', methods=['GET'])
@app.route('/index/<int:page>', methods=['GET'])
def index(page=1):
    products = models.Product.get_all_products().paginate(page, PRODUCTS_PER_PAGE, False)
    return render_template('index.html', title='SIROOP', products=products)


@app.route('/display/<int:id>', methods=['GET'])
def show(id):
    product = models.Product.get_product(id)
    return render_template('show.html', title='SIROOP', product=product)


@app.context_processor
def helper_funcs():
    def random_image_path():
        return "images/" + str(randint(1, 3)) + ".jpg"

    return dict(random_image_path=random_image_path)
