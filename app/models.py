from app import db


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    availability = db.Column(db.String(120))
    category = db.Column(db.String(120), index=True)
    categoryID = db.Column(db.String(120), index=True)
    cnetID = db.Column(db.String(120), index=True)
    code = db.Column(db.String(120), index=True)
    currency = db.Column(db.String(64))
    deliveryMode = db.Column(db.String(64))
    description = db.Column(db.String(1024))
    ean = db.Column(db.String(120))
    employeeDiscount = db.Column(db.Float, index=True)
    imageURL = db.Column(db.String(120))
    location = db.Column(db.String(120))
    manufacturer = db.Column(db.String(120))
    manufacturerAID = db.Column(db.String(120))
    maxOrderQuantity = db.Column(db.Integer)
    merchant = db.Column(db.String(64))
    name = db.Column(db.String(120), index=True)
    price = db.Column(db.Float)
    shortDescription = db.Column(db.String(120))
    stock = db.Column(db.Integer)
    vat = db.Column(db.Float)
    warranty = db.Column(db.Integer)
    weight = db.Column(db.Float)
    weightUnit = db.Column(db.String(64))
    features = db.relationship('Feature', backref='product', lazy='dynamic')

    @staticmethod
    def get_all_products():
        return Product.query.join(Feature, (Feature.id == Product.id))

    @staticmethod
    def get_product(id):
        return Product.query.get(id)

    @staticmethod
    def product_count():
        return Product.query.count()

    def __repr__(self):
        return '<Product %r>' % (self.name)


class Feature(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    label = db.Column(db.String(140))
    value = db.Column(db.String(140))
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))

    def __repr__(self):
        return '<Feature %r:%r>' % (self.label, self.value)
