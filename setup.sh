#!/bin/sh

echo ""
echo "Installing Dependencies"
echo "======================="
echo ""
pip3 install -r requirements.txt

echo ""
echo "Preparing product catalogue"
echo "======================="
echo ""

python3 setup.py