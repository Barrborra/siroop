from db_create import db_create
from db_migrate import db_migrate
from parser import Parser
from app.models import Product

# create db
print("Creating database")
db_create()

# migrate db
print("Migrating database")
db_migrate()

if Product.product_count() == 0:
    # parse product xml if products table is empty
    print("Parsing product feed")
    p = Parser()
    p.parse_product_feed('./data/feed.xml')
