from inspect import getsourcefile
import os.path as path, sys

current_dir = path.dirname(path.abspath(getsourcefile(lambda: 0)))
sys.path.insert(0, current_dir[:current_dir.rfind(path.sep)])

from parser import Parser

sys.path.pop(0)

import unittest
from mock_db import MockDb

PRODUCT_KEYS = ['name', 'category', 'price', 'description', 'shortDescription', 'currency']
FEATURE_KEYS = ['label', 'value']


# assert_fields asserts the presence of object fields on object
def assert_fields(items, item_fields):
    for item in items:
        for k in item_fields:
            val = getattr(item, k, False)
            if val is False:
                self.fail('missing  attribute: ' + k)


class ParseFeedTest(unittest.TestCase):
    def setUp(self):
        # create mock db, initialize parser with mock storage
        self.db = MockDb()
        self.p = Parser(self.db)

    def tearDown(self):
        # clear our db
        self.mock_db = None
        self.p = None

    def test_parse_full_record(self):
        self.p.parse_product_feed("data/test_feed_full_record.xml")
        products = self.db.session.objects['Product']
        features = self.db.session.objects['Feature']
        self.assertEqual(1, len(products))
        self.assertEqual(2, len(features))
        assert_fields(products, PRODUCT_KEYS)
        assert_fields(features, FEATURE_KEYS)

    def test_parse_full_records(self):
        products = self.p.parse_product_feed("data/test_feed_full_records.xml")
        self.assertEqual(2, len(self.db.session.objects['Product']))
        self.assertEqual(35, len(self.db.session.objects['Feature']))

    def test_parse_empty_features(self):
        products = self.p.parse_product_feed("data/test_feed_empty_features.xml")
        self.assertEqual(1, len(self.db.session.objects['Product']))
        self.assertEqual(0, len(self.db.session.objects['Feature']))


if __name__ == '__main__':
    unittest.main()
