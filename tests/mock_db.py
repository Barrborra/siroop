from collections import defaultdict


# implement mock for sqlalchemy db used in testing
class MockDb:
    def __init__(self):
        self.session = MockSession()


class MockSession:
    def __init__(self):
        self.objects = defaultdict(list)

    # add object into a dict of list, where key is object type
    def add(self, obj):
        obj_type = type(obj).__name__
        self.objects[obj_type].append(obj)

    def commit(self):
        pass

    def objects(self):
        return self.objects
